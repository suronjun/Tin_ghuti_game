
/*........game start condition................*/


$('#start').click(function() {
    $('#p1,#p2,#p3').addClass("blue active fill");
	$('#p4,#p5,#p6').addClass("blank");
	$('#p7,#p8,#p9').addClass("red fill");
	$(this).addClass("nvis"); 
	$('#end').removeClass("nvis");
});
$('#end').click(function() {
    $('.common').removeClass("selected red blue active fill blank possilbe");
	$(this).addClass("nvis"); 
	$('#start').removeClass("nvis");
});
$('.play_again').click(function() {
    $('.common').removeClass("selected red blue active fill blank possilbe");
	$('.finale').addClass("nvis"); 
	$('#start').removeClass("nvis");
	
});

/*.......game functions.........*/

$('#p1').click(function() {
    
	if ($(this).hasClass("active")){
        $('.selected').removeClass("selected");
		$(this).addClass("selected"); 
		$('.possible').removeClass("possible");
	    $('#p2,#p4,#p5').addClass("possible");
	    $('.fill').removeClass("possible");
    } 
	else if ($(this).hasClass("possible")){
                 $('.possible').removeClass("possible");
				 if ($('.selected').hasClass("blue")){
                         $(this).addClass("blue fill"); 
						 $('.blue').removeClass("active");
						 $('.red').addClass("active");
						 $('.selected').addClass("blank");
						 $('.selected').removeClass("blue active selected fill");
						 
                 } 
		    else if ($('.selected').hasClass("red")){
                              $(this).addClass("red fill");
							  $('.red').removeClass("active");
							  $('.blue').addClass("active");
							  $('.selected').addClass("blank");
							  $('.selected').removeClass("red active selected fill");
				 };
			     
				 
		 }
});

$('#p2').click(function() {
    
	if ($(this).hasClass("active")){
        $('.selected').removeClass("selected");
		$(this).addClass("selected"); 
		$('.possible').removeClass("possible");
	    $('#p1,#p3,#p5').addClass("possible");
	    $('.fill').removeClass("possible");
    } 
	else if ($(this).hasClass("possible")){
                 $('.possible').removeClass("possible");
				 if ($('.selected').hasClass("blue")){
                         $(this).addClass("blue fill"); 
						 $('.blue').removeClass("active");
						 $('.red').addClass("active");
						 $('.selected').addClass("blank");
						 $('.selected').removeClass("blue active selected fill");
						 
                 } 
		    else if ($('.selected').hasClass("red")){
                              $(this).addClass("red fill");
							  $('.red').removeClass("active");
							  $('.blue').addClass("active");
							  $('.selected').addClass("blank");
							  $('.selected').removeClass("red active selected fill");
				 };
			     
				 
		 }
});

$('#p3').click(function() {
    
	if ($(this).hasClass("active")){
        $('.selected').removeClass("selected");
		$(this).addClass("selected"); 
		$('.possible').removeClass("possible");
	    $('#p2,#p5,#p6').addClass("possible");
	    $('.fill').removeClass("possible");
    } 
	else if ($(this).hasClass("possible")){
                 $('.possible').removeClass("possible");
				 if ($('.selected').hasClass("blue")){
                         $(this).addClass("blue fill"); 
						 $('.blue').removeClass("active");
						 $('.red').addClass("active");
						 $('.selected').addClass("blank");
						 $('.selected').removeClass("blue active selected fill");
						 
                 } 
		    else if ($('.selected').hasClass("red")){
                              $(this).addClass("red fill");
							  $('.red').removeClass("active");
							  $('.blue').addClass("active");
							  $('.selected').addClass("blank");
							  $('.selected').removeClass("red active selected fill");
				 };
			     
				 
		 }
});

$('#p4').click(function() {
    
	if ($(this).hasClass("active")){
        $('.selected').removeClass("selected");
		$(this).addClass("selected"); 
		$('.possible').removeClass("possible");
	    $('#p1,#p5,#p7').addClass("possible");
	    $('.fill').removeClass("possible");
    } 
	else if ($(this).hasClass("possible")){
                 $('.possible').removeClass("possible");
				 if ($('.selected').hasClass("blue")){
                         $(this).addClass("blue fill"); 
						 $('.blue').removeClass("active");
						 $('.red').addClass("active");
						 $('.selected').addClass("blank");
						 $('.selected').removeClass("blue active selected fill");
						 
                 } 
		    else if ($('.selected').hasClass("red")){
                              $(this).addClass("red fill");
							  $('.red').removeClass("active");
							  $('.blue').addClass("active");
							  $('.selected').addClass("blank");
							  $('.selected').removeClass("red active selected fill");
				 };
			     
				 
		 }
});

$('#p5').click(function() {
    
	if ($(this).hasClass("active")){
        $('.selected').removeClass("selected");
		$(this).addClass("selected"); 
		$('.possible').removeClass("possible");
	    $('#p1,#p2,#p3,#p4,#p6,#p7,#p8').addClass("possible");
	    $('.fill').removeClass("possible");
    } 
	else if ($(this).hasClass("possible")){
                 $('.possible').removeClass("possible");
				 if ($('.selected').hasClass("blue")){
                         $(this).addClass("blue fill"); 
						 $('.blue').removeClass("active");
						 $('.red').addClass("active");
						 $('.selected').addClass("blank");
						 $('.selected').removeClass("blue active selected fill");
						 
                 } 
		    else if ($('.selected').hasClass("red")){
                              $(this).addClass("red fill");
							  $('.red').removeClass("active");
							  $('.blue').addClass("active");
							  $('.selected').addClass("blank");
							  $('.selected').removeClass("red active selected fill");
				 };
			     
				 
		 }
});

$('#p6').click(function() {
    
	if ($(this).hasClass("active")){
        $('.selected').removeClass("selected");
		$(this).addClass("selected"); 
		$('.possible').removeClass("possible");
	    $('#p3,#p5,#p9').addClass("possible");
	    $('.fill').removeClass("possible");
    } 
	else if ($(this).hasClass("possible")){
                 $('.possible').removeClass("possible");
				 if ($('.selected').hasClass("blue")){
                         $(this).addClass("blue fill"); 
						 $('.blue').removeClass("active");
						 $('.red').addClass("active");
						 $('.selected').addClass("blank");
						 $('.selected').removeClass("blue active selected fill");
						 
                 } 
		    else if ($('.selected').hasClass("red")){
                              $(this).addClass("red fill");
							  $('.red').removeClass("active");
							  $('.blue').addClass("active");
							  $('.selected').addClass("blank");
							  $('.selected').removeClass("red active selected fill");
				 };
			     
				 
		 }
});

$('#p7').click(function() {
    
	if ($(this).hasClass("active")){
        $('.selected').removeClass("selected");
		$(this).addClass("selected"); 
		$('.possible').removeClass("possible");
	    $('#p4,#p5,#p8').addClass("possible");
	    $('.fill').removeClass("possible");
    } 
	else if ($(this).hasClass("possible")){
                 $('.possible').removeClass("possible");
				 if ($('.selected').hasClass("blue")){
                         $(this).addClass("blue fill"); 
						 $('.blue').removeClass("active");
						 $('.red').addClass("active");
						 $('.selected').addClass("blank");
						 $('.selected').removeClass("blue active selected fill");
						 
                 } 
		    else if ($('.selected').hasClass("red")){
                              $(this).addClass("red fill");
							  $('.red').removeClass("active");
							  $('.blue').addClass("active");
							  $('.selected').addClass("blank");
							  $('.selected').removeClass("red active selected fill");
				 };
			     
				 
		 }
});

$('#p8').click(function() {
    
	if ($(this).hasClass("active")){
        $('.selected').removeClass("selected");
		$(this).addClass("selected"); 
		$('.possible').removeClass("possible");
	    $('#p5,#p7,#p9').addClass("possible");
	    $('.fill').removeClass("possible");
    } 
	else if ($(this).hasClass("possible")){
                 $('.possible').removeClass("possible");
				 if ($('.selected').hasClass("blue")){
                         $(this).addClass("blue fill"); 
						 $('.blue').removeClass("active");
						 $('.red').addClass("active");
						 $('.selected').addClass("blank");
						 $('.selected').removeClass("blue active selected fill");
						 
                 } 
		    else if ($('.selected').hasClass("red")){
                              $(this).addClass("red fill");
							  $('.red').removeClass("active");
							  $('.blue').addClass("active");
							  $('.selected').addClass("blank");
							  $('.selected').removeClass("red active selected fill");
				 };
			     
				 
		 }
});

$('#p9').click(function() {
    
	if ($(this).hasClass("active")){
        $('.selected').removeClass("selected");
		$(this).addClass("selected"); 
		$('.possible').removeClass("possible");
	    $('#p5,#p6,#p8').addClass("possible");
	    $('.fill').removeClass("possible");
    } 
	else if ($(this).hasClass("possible")){
                 $('.possible').removeClass("possible");
				 if ($('.selected').hasClass("blue")){
                         $(this).addClass("blue fill"); 
						 $('.blue').removeClass("active");
						 $('.red').addClass("active");
						 $('.selected').addClass("blank");
						 $('.selected').removeClass("blue active selected fill");
						 
                 } 
		    else if ($('.selected').hasClass("red")){
                              $(this).addClass("red fill");
							  $('.red').removeClass("active");
							  $('.blue').addClass("active");
							  $('.selected').addClass("blank");
							  $('.selected').removeClass("red active selected fill");
				 };
			     
				 
		 }
});


/*.........victory condition...............*/

 $('.common').click(function() {
    
	 if ($('#p1').hasClass("blue") && $('#p5').hasClass("blue") && $('#p9').hasClass("blue")){
             $('#blue_win').removeClass("nvis"); 
		     $('#blue_win').addClass("yvis");
     } 
else if ($('#p1').hasClass("red") && $('#p5').hasClass("red") && $('#p9').hasClass("red")){
             $('#red_win').removeClass("nvis"); 
		     $('#red_win').addClass("yvis");
	
     }
     if ($('#p2').hasClass("blue") && $('#p5').hasClass("blue") && $('#p8').hasClass("blue")){
             $('#blue_win').removeClass("nvis"); 
		     $('#blue_win').addClass("yvis");
     } 
else if ($('#p2').hasClass("red") && $('#p5').hasClass("red") && $('#p8').hasClass("red")){
             $('#red_win').removeClass("nvis"); 
		     $('#red_win').addClass("yvis");
	
     }
	 if ($('#p3').hasClass("blue") && $('#p5').hasClass("blue") && $('#p7').hasClass("blue")){
             $('#blue_win').removeClass("nvis"); 
		     $('#blue_win').addClass("yvis");
     } 
else if ($('#p3').hasClass("red") && $('#p5').hasClass("red") && $('#p7').hasClass("red")){
             $('#red_win').removeClass("nvis"); 
		     $('#red_win').addClass("yvis");
	
     }
	 if ($('#p4').hasClass("blue") && $('#p5').hasClass("blue") && $('#p6').hasClass("blue")){
             $('#blue_win').removeClass("nvis"); 
		     $('#blue_win').addClass("yvis");
     } 
else if ($('#p4').hasClass("red") && $('#p5').hasClass("red") && $('#p6').hasClass("red")){
             $('#red_win').removeClass("nvis"); 
		     $('#red_win').addClass("yvis");
	
  };
})